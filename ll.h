#ifndef __LL_H
#define __LL_H

#ifdef __cplusplus
  extern "C" {
#endif

#include "cmsis.h"
#include "ll_gpio.h"
/* wrapper definitions to make sure we accomplish notation standar */
/* --------------------------------------------------------------------------------------------- */
/* <driver> module (stm32f0xx_ll_<driver>.h) -------------------------------------------------- */
    /* Defines */
    /* Macros */
    /* Typedefs */
    /* Structure elements */
    /* Functions */


#ifdef __cplusplus
}
#endif

#endif
