#ifndef __ll_gpio_h__
#define __ll_gpio_h__

#ifdef __cplusplus
  extern "C" {
#endif


/* gpio module (stm32f0xx_ll_gpio.h) -------------------------------------------------- */
    /* Defines */
#define _ll_gpio_pin_0         LL_GPIO_PIN_0
    /* Macros */
    /* Typedefs */
    /* Structure elements */
    /* Functions */
#define ll_gpio_init            LL_GPIO_Init
#define ll_gpio_structInit      LL_GPIO_StructInit
#define ll_gpio_setPinMode      LL_GPIO_SetPinMode


#ifdef __cplusplus
}
#endif

#endif
